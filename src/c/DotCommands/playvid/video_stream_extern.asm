;; EXTERNAL VARIABLES FROM DOT COMPILE
;; These variables are not part of the player code block

;; *** MUST BE CONSISTENT WITH MAP FILE FROM MAIN DOT COMPILE

;; these defines should come from the automatically generated
;; playvidp.def via include but the current z88dk is bugged
;; so this list needs to be maintained manually using defines
;; coming from the map file

; esx stream variable

defc __esx_stream_card_flags = 0x3e13

; audio modification for 60 Hz playback

defc isr_install_60          = 0x8020

; video player variables

defc _vbi_counter            = 0x80f7   ; time passage in frames

defc _w_layer2_page_count    = 0x80f8   ; number of pages in L2 screen (local)  \  must be
defc _w_layer2_page          = 0x80f9   ; destination L2 page                   |  sequential
defc _w_layer2_palette       = 0x80fa   ; destination palette                   /  in memory
defc _w_audio_buffer         = 0x80fb   ; destination audio buffer address (local)

defc _v_fragment_remaining   = 0x80fd   ; sectors remaining in current file fragment (32-bit)
defc _v_file_fragment        = 0x8101   ; pointer to next file fragment (16-bit)
defc _v_frame_count          = 0x8103   ; number of video frames to play (32-bit)
